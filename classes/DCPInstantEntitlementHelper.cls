/**
* @description
* Class: DCPInstantEntitlementHelper
* Purpose: To process trial conversion or create Enterprise Trials for Instant Entitlement
* Created: 09/09/2021
* Author: Kunal Mishra
* 
* CHANGE HISTORY
* =============================================================================
* Date         Name             JIRA        Description
* 09/09/2021   Kunal M         DCP-4233    Created
* =============================================================================
*/
public with sharing class DCPInstantEntitlementHelper {
    public static String oppID;
    public static String ssdID;
    public Static String supportEmail = Static_Values__c.getValues('OrderProcessSupportEmail').Value__c; // Email address of support team member
    public static boolean createTrials = false;
     /********************************************************************************************************
* @description mapConfigDetails: Method to map the fields which need to be updated on Product Configuration/Cart Record
*  @param leadRecId 
*  @param quoteRecId
*  @param onePassGUID

*********************************************************************************************************/  
    
    // public static void processInstantEntitlement(DCPConvertLeadHelperTwo.ProcessTRStoreWrapperDetails ptwd){
    @future   
    public static void processInstantEntitlement(string leadRecId,string quoteRecId,string onePassGUID){
         
        try{
        
        list<Lead> leadDetails = new list<Lead>();
        list<Online_Contacts__c> lisOfOlContacts = new list<Online_Contacts__c>();
        list<Source_System_Detail__c> ssdDetails = new list<Source_System_Detail__c>();
        list<String> listOfStringIDs = new list<String>();
        list<Apttus_Proposal__Proposal__c> quoteDetails = new list<Apttus_Proposal__Proposal__c>();
        list<Opportunity> oppDetails = new list<Opportunity>();
        ID quoteId = ID.valueOf(quoteRecId);
        ID leadId = ID.valueOf(leadRecId);
        String onePassID = onePassGUID;
        
        System.debug(LoggingLevel.Info, 'Inside Process TRS Order');            
        
       if(leadId!=null && quoteId!=null){
            string quoteQuery = 'SELECT Id,Is_NSE_SM__c,Name,Apttus_Proposal__Opportunity__c, APTS_SSD_Sold_To__c,Apttus_Proposal__Primary_Contact__c,Apttus_Proposal__Account__c  FROM Apttus_Proposal__Proposal__c WHERE Id =: quoteId';
            quoteDetails = database.query(quoteQuery);
            oppID = quoteDetails[0].Apttus_Proposal__Opportunity__c;
            string leadQuery = 'SELECT Id, Name,firstname, lastname, Email, isconverted, convertedAccountid, convertedcontactid,Product_Configuration__c, convertedopportunityId, convertedopportunity.name, convertedopportunity.Source_System_Detail__c,Lead_Region__c,phone,(select id, name, Email__c, Last_Name__c FROM Online_Contacts__r), (Select id, name FROM Customer_Master_Contacts__r ) from Lead WHERE id =:leadId AND isConverted=true';  
            leadDetails = database.query(leadQuery);
            ssdID = quoteDetails[0].APTS_SSD_Sold_To__c;
            if(String.isNotBlank(ssdID)){
                string ssdQuery = 'SELECT ID,Source_System_Account_Number__c FROM Source_System_Detail__c WHERE ID =:ssdID';
                ssdDetails = database.query(ssdQuery);
            }
            oppDetails = [SELECT ID,Is_Digital_New_Sales__c FROM Opportunity WHERE ID=:oppID WITH SECURITY_ENFORCED];
            System.debug(LoggingLevel.INFO, 'Create Entitlement');
            for(Online_Contacts__c oContact : [SELECT ID,First_Name__c,Last_Name__c,Type__c,Email__c FROM Online_Contacts__c WHERE QuoteId__c=:quoteID AND  Type__c = 'Admin' WITH SECURITY_ENFORCED]){
                lisOfOlContacts.add(oContact);
            }
            if(!lisOfOlContacts.isEmpty() && oppDetails[0].Is_Digital_New_Sales__c == true){
                Id newTrialId = createEntTrial(leadDetails,ssdDetails,quoteDetails );
                 
                System.debug(LoggingLevel.INFO, 'New Trial'+newTrialId);
                listOfStringIDs.add(newTrialId);
                listOfStringIDs.add(quoteID);
                createEntTrialContact(lisOfOlContacts,listOfStringIDs,onePassID);
                createEntTrialProduct(quoteId,newTrialId);
                string trialSfId = newTrialId;
                List<Trial_Contact__c> trialContacts = [SELECT ID FROM Trial_Contact__c WHERE Trials__c = :newTrialId WITH SECURITY_ENFORCED];
                List<Trial_Product__c> trialProducts = [SELECT ID FROM Trial_Product__c WHERE Trial__c = :newTrialId WITH SECURITY_ENFORCED];
                ODTDG_EntitleTrialCustomerRecordsService.createContactProduct(trialContacts,trialProducts,trialSfId);
            }
        
            }  
        }
        catch(Exception ex){
            Map<String,String> errorHandlerMap = new Map<String,String>{'objectName'=>'Quote/Proposal','expMsg'=>String.valueof(ex.getLineNumber())+','+ex.getMessage(),'webServiceName'=>'UEUSL QuoteSyncUp','requestType'=>'cal for Entitlement from Class DCPQuoteSyncupController','reqMsg'=>ex.getStackTraceString(),'module'=>'UEStore','supportEmail'=>supportEmail,'toSupport'=>'false'};
            DCPNewSaleUtilityCls.logException(errorHandlerMap);
        }
        
     }
     
          /********************************************************************************************************
* @description futureCallToEntitlement : future Method to do callout to Entitlement
* @param proposalID - quoteID
*********************************************************************************************************/ 
     
        
        public static void futureCallToEntitlement(String proposalID){
        System.debug(LoggingLevel.INFO, 'Inside callout');
        
        Id accountId;Id ssdRecId;String sapAccNumber;
        String countryCode;
        String stateCode;
        String marketSegment;
        String accountName;
        String orgTypeSegment;
        String industryCode;
        ID quoteID = ID.valueOf(proposalID);
                
        list<Ent_Trial__c> listOfEntTrial = [SELECT Id,Account__c,External_Account_Number__c,Opportunity__c,Convert_to_Sale__c,Digital_Paid_Order_Quote_ID__c,Instant_Entitlement__c,Organization_Type__c,Market_Segment__c,Account_Name__c FROM Ent_Trial__c WHERE Digital_Paid_Order_Quote_ID__c = :quoteId AND Convert_to_Sale__c=false AND Instant_Entitlement__c = true WITH SECURITY_ENFORCED];
        
        list<Apttus_Proposal__Proposal__c> accDetails = new list<Apttus_Proposal__Proposal__c>();
        list<Source_System_Detail__c> ssdRecDetails = new list<Source_System_Detail__c>();
        
        string accFromQuoteQuery = 'SELECT ID,Apttus_Proposal__Account__c,APTS_SSD_Sold_To__c,APTS_Approval_Segment__c FROM Apttus_Proposal__Proposal__c WHERE Id =:quoteID';
        accDetails = database.query(accFromQuoteQuery);
        accountId = accDetails[0].Apttus_Proposal__Account__c;
        ssdRecId = accDetails[0].APTS_SSD_Sold_To__c;
        
        orgTypeSegment = DCPConvertLeadHelperTwo.getOrgTypeSegment(accDetails[0].APTS_Approval_Segment__c);
        
        
        string ssdRecQuery = 'SELECT Id,Source_System_Account_Number__c,LCRM_Industry__c,Account_Name__r.Name,LCRM_Country_Code__c,State__c FROM Source_System_Detail__c WHERE Id = :ssdRecId';
        ssdRecDetails = database.query(ssdRecQuery);
        sapAccNumber = ssdRecDetails[0].Source_System_Account_Number__c;        
        countryCode = ssdRecDetails[0].LCRM_Country_Code__c;
        stateCode = ssdRecDetails[0].State__c;
        marketSegment = ssdRecDetails[0].LCRM_Industry__c;
        accountName = ssdRecDetails[0].Account_Name__r.Name;
        industryCode = DCPConvertLeadHelperTwo.mapIndustryCode(marketSegment);
        
        
        
        

        if(!listOfEntTrial.isEmpty()){
            ID trialID = listOfEntTrial[0].Id;
                listOfEntTrial[0].Account__c = accountId;
                listOfEntTrial[0].External_Account_Number__c = sapAccNumber;
                listOfEntTrial[0].Organization_Type__c = orgTypeSegment;
                listOfEntTrial[0].Market_Segment__c = marketSegment;
                listOfEntTrial[0].Account_Name__c = accountName;
                listOfEntTrial[0].Industry_code__c = industryCode;
                
            
                list<Id> listOfConIds = new list<Id>();
                list<Id> listOfProdIds = new list<Id>();
                list<Trial_Contact__c> listOFTrialConToUpdate = new list<Trial_Contact__c>();
                
                for(Trial_Contact__c tContact : [SELECT Id,Trials__c,Country__c,stateCode__c FROM Trial_Contact__c WHERE Trials__c = :trialID WITH SECURITY_ENFORCED]){
                    tContact.Country__c = countryCode;
                    tContact.stateCode__c = stateCode;
                    listOFTrialConToUpdate.add(tContact);
                    listOfConIds.add(tContact.Id);
                }
                for(Trial_Product__c tProduct : [SELECT Id,Trial__c FROM Trial_Product__c WHERE Trial__c = :trialID WITH SECURITY_ENFORCED]){
                    listOfProdIds.add(tProduct.Id);
                }
                
                if(Schema.sObjectType.Ent_Trial__c.isUpdateable()){Database.update(listOfEntTrial, false);}
                if(Schema.sObjectType.Trial_Contact__c.isUpdateable()){Database.update(listOFTrialConToUpdate, false);}
                
            callPaidOrderInFuture(trialID, listOfConIds, listOfProdIds);//OmniCalltoSubmitEntitlement.callToSubmitEntitlement(trialID, listOfConIds, listOfProdIds);
            
        
        }
        else{if(!Test.isRunningTest()){system.enqueueJob(new DCPDocusignDelayProcess(quoteID,'completeEnv'));}}
        }
    /********************************************************************************************************
* @description updateEntTrial : to process updates after callout is success
* @param trialID
* @param listOfConIds
* @param listOfProdIds
*********************************************************************************************************/ 
@future(callout=true)
    public static void callPaidOrderInFuture(Id trialID,list<Id> listOfConIds,list<Id> listOfProdIds){
        if(!Test.isRunningTest()){ OmniCalltoSubmitEntitlement.callToSubmitEntitlement(trialID, listOfConIds, listOfProdIds);}
    }
        
        /********************************************************************************************************
* @description updateEntTrial : to process updates after callout is success
* @param listOfTrialIds - List<Id>
*********************************************************************************************************/ 
        @InvocableMethod
        public static void updateEntTrialPostCallBack(List<Id> listOfTrialIds){
            system.debug(LoggingLevel.INFO,'Update Trial'+listOfTrialIds);
            ID quoteID;ID oppID;ID trialId;
            list<Ent_Trial__c> updateEntTriallist = new list<Ent_Trial__c>();
            trialId = listOfTrialIds[0];
            for(Ent_Trial__c oTrial : [SELECT ID,Convert_to_Sale__c,Opportunity__c,Digital_Paid_Order_Quote_ID__c,Trial_Status__c FROM Ent_Trial__c WHERE ID IN  :listOfTrialIds WITH SECURITY_ENFORCED ]){
                oTrial.Convert_to_Sale__c = true;
                quoteID = oTrial.Digital_Paid_Order_Quote_ID__c;
                if(oTrial.Trial_Status__c == 'Active' ){
                updateEntTriallist.add(oTrial);}
            }
            
            for(Apttus_Proposal__Proposal__c oProposal : [SELECT ID,Apttus_Proposal__Opportunity__c FROM Apttus_Proposal__Proposal__c WHERE Id= :quoteID WITH SECURITY_ENFORCED]){
                oppID = oProposal.Apttus_Proposal__Opportunity__c;
            }

            list<Online_Contacts__c> olConList = new list<Online_Contacts__c>();
            string olQuery = 'SELECT Email__c,ContactID__c FROM Online_Contacts__c WHERE QuoteId__c=:quoteID';
            olConList = database.query(olQuery);
            olConList = DCPLeadOnlineContactHelper.mapOnlineContactNumber(trialId,olConList);
            if(!updateEntTriallist.isEmpty() && Schema.sObjectType.Ent_Trial__c.isUpdateable()){
                updateEntTriallist[0].Opportunity__c = oppID;
                Database.update(updateEntTriallist, false);
            }
            if(!Test.isRunningTest()){system.enqueueJob(new DCPDocusignDelayProcess(quoteID,'completeEnv'));}
            DCPLeadOnlineContactHelper.updateOnePassID(trialId);
            DCPLeadOnlineContactHelper.updateOnlineCon(olConList);
            
            
        }
/********************************************************************************************************
* @description createEntTrial : Method to create Enterprise Trials
* @param leadDetails - list<Lead>
* @param ssdDetail - list<Source_System_Detail__c> ssdDetail
* @param quoteDetails
* @return Id

*********************************************************************************************************/ 
     public static Id createEntTrial(list<Lead> leadDetails,list<Source_System_Detail__c> ssdDetail,
                                      list<Apttus_Proposal__Proposal__c> quoteDetails){
        list<Ent_Trial__c> newEntTriallist = new list<Ent_Trial__c>();
        ID newTrialID;
        if(!leadDetails.isEmpty() && !ssdDetail.isEmpty()){
        Ent_Trial__c tempTrial = new Ent_Trial__c();
        
        tempTrial.Trial_Start_Date__c = System.Today();
        tempTrial.Trial_End_Date__c = System.Today() + 7;
        tempTrial.Trial_Status__c = 'Draft';
        tempTrial.Trial_Source__c = 'Digital';
        tempTrial.Lead__c = leadDetails[0].Id;
        tempTrial.Digital_Paid_Order_Quote_ID__c = quoteDetails[0].Id;
        tempTrial.Profile_Type__c = 'NEW_CUSTOMER';
       
        tempTrial.Instant_Entitlement__c = true;

        
        newEntTriallist.add(tempTrial);
        Database.SaveResult[] srlist = Database.insert(newEntTriallist, false);

        for (Database.SaveResult sr : srlist) {
            if (sr.isSuccess()) {
                newTrialID = sr.getId();
            }
            
        }
        }
        
        return newTrialID;

     }
     /********************************************************************************************************
* @description createEntTrialContact: Method to create Trial Contacts
* @param lisOfOlContacts - list<Online_Contacts__c>
* @param listOfStringIDs - Id
* @param onePassID - Id
* @return - list<Id>


*********************************************************************************************************/ 
    public static list<Id> createEntTrialContact(list<Online_Contacts__c> lisOfOlContacts,list<String> listOfStringIDs,String onePassID){
        ID trialID = ID.valueOf(listOfStringIDs[0]);
        list<Trial_Contact__c> newTrialConlist = new list<Trial_Contact__c>();
        list<String> listOfEmails = new list<String>();
        list<ID> listOfConIds = new list<ID>();
        for(Online_Contacts__c oContact : lisOfOlContacts){
            Trial_Contact__c newTrialContact = new Trial_Contact__c();

            newTrialContact.Email__c = oContact.Email__c;
            newTrialContact.First_Name__c = oContact.First_Name__c;
            newTrialContact.Last_Name__c = oContact.Last_Name__c;
            newTrialContact.Name = oContact.First_Name__c + ' ' + oContact.Last_Name__c;
            newTrialContact.Trials__c = trialID;
            newTrialContact.One_Pass_User_Name__c = onePassID;
            newTrialContact.Status__c = 'Accepted';
            newTrialContact.Request_Type__c = 'PAID';
            newTrialContact.Primary__c= true;
            newTrialConlist.add(newTrialContact);
            listOfEmails.add(oContact.Email__c);
        }

        Database.SaveResult[] srlist = Database.insert(newTrialConlist, false);
        for (Database.SaveResult sr : srlist) {
            if (sr.isSuccess()) {
                listOfConIds.add(sr.getId());
            }
        }


        return listOfConIds;


    }
/********************************************************************************************************
* @description createEntTrialProduct : Method to create Trial Products
* @param quoteID - Id
* @param trialID - Id
* @return list<String>

*********************************************************************************************************/
    public static list<Id> createEntTrialProduct(ID quoteID,ID trialID){
    system.debug(LoggingLevel.INFO, 'Quote ID'+quoteID);
        list<Trial_Product__c> listOfTrialProd = new list<Trial_Product__c>();
        list<ID> listOfProdIds = new list<ID>();

        for(Apttus_Proposal__Proposal_Line_Item__c oLineItem : [SELECT ID,Apttus_Proposal__Product__c,Apttus_Proposal__Product__r.Name,APTS_Product_Name__c,APTS_Product_Family__c,Apttus_QPConfig__BaseProductId__r.ProductCode,Apttus_QPConfig__BaseProductId__c FROM Apttus_Proposal__Proposal_Line_Item__c WHERE Apttus_Proposal__Proposal__c = :quoteID WITH SECURITY_ENFORCED]){
            System.debug(LoggingLevel.INFO, 'Queried Line Item'+oLineItem );
            Trial_Product__c oTrialProd = new Trial_Product__c();
            oTrialProd.Name = DCPConvertLeadHelperTwo.getProductName(oLineItem.APTS_Product_Name__c);
            oTrialProd.Product_Name__c = oLineItem.Apttus_Proposal__Product__c;
            oTrialProd.Base_Material_Number__c = oLineItem.Apttus_QPConfig__BaseProductId__c;
            
            oTrialProd.Trial__c = trialID;
            if(oLineItem.Apttus_QPConfig__BaseProductId__r.ProductCode!=null)
            {   oTrialProd.Relationship_Type__c = 'Accessory Product';}
            else{   
                oTrialProd.Relationship_Type__c = 'Plan to Plan';
            }
        

            listOfTrialProd.add(oTrialProd);

        }

        Database.SaveResult[] srlist = Database.insert(listOfTrialProd, false);
        for (Database.SaveResult sr : srlist) {
            if (sr.isSuccess()) {
                listOfProdIds.add(sr.getId());
            }
        }

        return listOfProdIds;
    }

}
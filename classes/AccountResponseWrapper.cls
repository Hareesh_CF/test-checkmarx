@SuppressWarnings('PMD')
/**
*@Comment : Account Response wrapper
*@description : Account Response wrapper
*/
public class AccountResponseWrapper {
    
    public cls_customerUpdateResponse customerUpdateResponse;
	public String status;
	public class cls_customerUpdateResponse {
		public cls_results[] results;
	}
	public class cls_results {
		public cls_MDMPartyData MDMPartyData;
	}
	public class cls_MDMPartyData {
		public cls_SourceCustomerData SourceCustomerData;
	}
	public class cls_SourceCustomerData {
		public cls_records[] records;
	}
	public class cls_records {
        public String GUID;
        public String Source_System_Customer_ID;
		public String Source_System_Name;
        /*
		public String AccountId;	//null
		public String Additional_Name_1;	//null
		public String Additional_Name_2;	//null
		public String Address_Line_1;	//171 LBS Marg
		public String Address_Line_2;	//null
		public String Address_Line_3;	//null
		public String Address_Line_4;	//null
		public String Address_Type;	//null
		public String BU_Classification_1;	//AP
		public String BU_Classification_2;	//A
		public String BU_Classification_3;	//null
		public String City_Name;	//New York
		public String Country_Code;	//null
		public String County_Name;	//null
		public String Created;	//2021-08-04 15:21:31.523
		public String Account_Currency;	//USD
		public String Customer_Number;	//100558774
		public String Customer_Type;	//Customer
		public String DUNS_Number;	//null
		public String Data_ID;	//15027418
		public String Data_Last_Updated;	//2021-08-05 01:58:42.327
		public String Email_Address;	//null
		public String Entity_Status;	//ACTIVE
		public String Entity_Type;	//O
		public String Firm_Number;	//null
		public String First_Name;	//null
		public String Full_Name;	//TEST AUG 0001
		public String GUID;	//15027418
		public String Hierarchy_Type;	//null
		public String Is_Entity_Sanctioned;	//null
		public String Is_Primary_Address;	//null
		public String Is_Record_Logically_Deleted;	//null
		public String Is_Tax_VAT_Exempt;	//null
		public String Last_Name;	//null
		public String Local_Additional_Name_1;	//null
		public String Local_Additional_Name_2;	//null
		public String Local_Address_Line_1;	//null
		public String Local_Address_Line_2;	//null
		public String Local_Address_Line_3;	//null
		public String Local_Address_Line_4;	//null
		public String Local_City_Name;	//null
		public String Local_Country_Code;	//null
		public String Local_County_Name;	//null
		public String Local_First_Name;	//null
		public String Local_Full_Name;	//null
		public String Local_Language;	//null
		public String Local_Last_Name;	//null
		public String Local_Middle_Name;	//null
		public String Local_State_Province_Code;	//null
		public String Local_Zip_Postal_Code;	//null
		public String Middle_Name;	//null
		public String Name_Suffix;	//null
		public String Phone_Number;	//null
		public String SAP_Number;	//null
		public String SBU_Name;	//TAX & ACCOUNTING
		public String Salesforce_Account_ID;	//null
		public String Source_System_Customer_ID;	//0013J00000HI1gWQAT
		public String Source_System_Name;	//LGLSFDCGLI
		public String State_Province_Code;	//NY
		public String Sub_SBU_Name;	//null
		public String VAT_Code;	//null
		public String Zip_Postal_Code;	//789456
		*/
	}
    
    public static AccountResponseWrapper parse(String json){
        
        return (AccountResponseWrapper) System.JSON.deserialize(json, AccountResponseWrapper.class);
        
    }
    
    //fromJSON obj = parse(json);
    
    
}
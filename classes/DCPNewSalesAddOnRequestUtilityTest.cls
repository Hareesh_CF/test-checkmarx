@isTest(seeAllData=false)
public class DCPNewSalesAddOnRequestUtilityTest {
    
     /** 
    * @description : Prepare test data
    */ 
    @testsetup static void testData(){
        
      //UEStoreUSLPlan__mdt[] usPlan =[SELECT MasterLabel,DeveloperName,Best_Plan__c,Better_Plan__c,Good_Plan__c,Jurisdiction__c,Segment__c,Sub_Segment__c,Brand__c FROM UEStoreUSLPlan__mdt limit 1];
      //String segment = usPlan[0].segment__c;
        LIST < Static_Values__c > staticCSList = new LIST < Static_Values__c > {
           
                new Static_Values__c(name = 'DCPAddonRuleEnabler',value__c = 'v2'),
				new Static_Values__c(name = 'OrderProcessSupportEmail', value__c = 'APP-CX-DIGITAL@thomson.com'),
                new Static_Values__c(name = 'NewSaleSellableFlag', value__c = 'Display and Sell')
           
        };
        insert staticCSList;
        
        Product2 prod = DCPNewSaleUtilityCls.createProduct('42076533');
        Insert prod;
    
        Product2 prod1 = DCPNewSaleUtilityCls.createProduct('42004986');
        Insert prod1;

        APTSECOM_Store__c store = new APTSECOM_Store__c(Name='Westlaw Classic Law Firm',APTSECOM_Customer_Pricing_Segment__c='Law Firm',
                                                       APTSECOM_Default_Contract_Term__c='3 Years',Type__c='New Sales',
                                                        APTSECOM_Max_No_Of_Attorneys__c=5);
        insert store;

        Apttus_Config2__ProductGroup__c plan = new Apttus_Config2__ProductGroup__c(Name = 'Good');
        insert plan;

        Apttus_Config2__ProductGroupMember__c member = new Apttus_Config2__ProductGroupMember__c(Apttus_Config2__Sequence__c=1,
                                                                                                Apttus_Config2__ProductId__c=prod.Id,
                                                                                                Apttus_Config2__ProductGroupId__c=plan.Id);
        
        insert member;
      
        APTSECOM_Product_Recommendation__c pRec = new APTSECOM_Product_Recommendation__c(APTSECOM_Source_Product__c=prod.Id,APTSECOM_Product__c=prod1.Id,
                                                                                            Plan__c=plan.Id,Can_only_be_added_with__c=plan.Id,APTSECOM_Store__c=store.Id,group_Name__c='abc');
        insert pRec;
       
        Product_Detail__c pdet = new Product_Detail__c(Group_Name__c='abc',DCP_store__c=store.id);
        insert pdet;
    }
    
    /** 
    * @description : Test method for ProductPlan Service
    */ 
     @isTest static void testforProductAddOnService() {
       Test.startTest();
       DCPNewSalesAddOnRequest.ProductRule prodRule = new DCPNewSalesAddOnRequest.ProductRule();
       Product2 prod= [select productcode,Name from product2 where productcode = '42076533'];
       Product2 prod1 = [select productcode,Name from product2 where productcode = '42004986'];
       Map<String,product2> addonProd = new Map<String,product2>{'42004986'=>prod};
       APTSECOM_Store__c  store = [select name from APTSECOM_Store__c  limit 1];
       Map<String,String> prodStore = new Map<String,String>{'42004986'=>'Westlaw Classic Law Firm'};
       APTSECOM_Product_Recommendation__c prodRecom = [SELECT APTSECOM_Product__c,Plan__c,Plan__r.Name,APTSECOM_Product__r.Free_Trial__c,
                               APTSECOM_Product__r.productcode,APTSECOM_Product__r.Name,APTSECOM_Product__r.Product_Offering__c,Group_Name__c
                               FROM APTSECOM_Product_Recommendation__c];
       product_detail__c prodDet = [Select id,DCP_AddonRuleSet__c, DCP_Store__r.Name, Group_Name__c from product_detail__c limit 1];
       system.assert(prodDet!=null, 'inserted');
	   system.debug(loggingLevel.INFO,'ProductCodes >>'+prod1+prod+store+prodRecom+prodDet+prodStore);
       DCPNewSalesAddOnRequestUtility.getGrpNames(addonProd,'Westlaw Classic','Law Firm');
       prodRule = DCPNewSalesAddOnRequestUtility.createRule(prod,new Map<string,product_detail__c>{'42004986'=>prodDet});
	   Map<String,String> errorHandlerMap = new Map<String,String>{'objectName'=>'Product Configuration','expMsg'=>'abc','webServiceName'=>'PromotionAPI','requestType'=>'Promotion Service: Manual/Auto Promotion','reqMsg'=>'Exception Class Name: TRStore_PromotionService','module'=>'UEStore','recId'=>'','supportEmail'=>'abc@sample.com','toSupport'=>'false'};
       DCPNewSaleUtilityCls.logException(errorHandlerMap);
         
       test.stopTest();
     }
     
    
    

}
@SuppressWarnings('PMD')
/**
*@Comment : Account Request wrapper
*@description : Account Request wrapper
*/
public class AccountRequestWrapper {
    public Cls_updateCustomerRequest updateCustomerRequest;
	public class Cls_updateCustomerRequest {
		public Cls_data[] data;
	}/*
	public class Cls_data {
		public String AccountId;	//
		public String Additional_Name_1;	//
		public String Additional_Name_2;	//
		public String Address_Line_1;	//11301 FARRAH UNT 917
		public String Address_Line_2;	//
		public String Address_Line_3;	//
		public String Address_Line_4;	//
		public String Address_Type;	//SELLING
		public String BU_Classification_1;	//AP
		public String BU_Classification_2;	//A
		public String BU_Classification_3;	//
		public String City_Name;	//AUSTIN
		public String Source_Country_Code;	//USA
		public String County_Name;	//null
		public String Account_Currency;	//null
		public String Customer_Number;	//100558774
		public String Customer_Type;	//Customer
		public String DUNS_Number;	//
		public String Email_Address;	//
		public String Entity_Status;	//ACTIVE
		public String Entity_Type;	//O
		public String Firm_Number;	//null
		public String First_Name;	//
		public String Full_Name;	//THOMAS MCSHERRY
		public String Hierarchy_Type;	//
		public String Is_Entity_Sanctioned;	//
		public String Is_Record_Logically_Deleted;	//0
		public String Is_Tax_VAT_Exempt;	//
		public String Last_Name;	//
		public String Local_Additional_Name_1;	//
		public String Local_Additional_Name_2;	//
		public String Local_Address_Line_1;	//
		public String Local_Address_Line_2;	//
		public String Local_Address_Line_3;	//
		public String Local_Address_Line_4;	//
		public String Local_City_Name;	//
		public String Local_Country_Code;	//
		public String Local_County_Name;	//
		public String Local_First_Name;	//
		public String Local_Full_Name;	//
		public String Local_Language;	//EN
		public String Local_Last_Name;	//
		public String Local_Middle_Name;	//
		public String Local_State_Province_Code;	//
		public String Local_Zip_Postal_Code;	//
		public String Middle_Name;	//
		public String Name_Suffix;	//
		public String Phone_Number;	//111111122
		public String SAP_Number;	//
		public String SBU_Name;	//TAX & ACCOUNTING
		public String Salesforce_Account_ID;	//
		public String Source_System_Customer_ID;	//903725803
		public String Source_System_Name;	//TRTAUNI
		public String State_Province_Code;	//TX
		public String Sub_SBU_Name;	//KS
		public String VAT_Code;	//null
		public String Zip_Postal_Code;	//78748-7908
	}*/
    
    public class Cls_data {
		public String AccountId;	//
		public String Address_Line_1;	//
		public String Address_Line_2;	//
		public String Address_Line_3;	//
		public String Address_Line_4;	//
		public String Address_Type;	//
		public String BU_Classification_1;	//
		public String BU_Classification_2;	//
		public String BU_Classification_3;	//
		public String City_Name;	//
		public String County_Name;	//
		public String Account_Currency;	//
		public String Customer_Group;	//
		public String Customer_Number;	//
		public String Customer_Type;	//
		public String DUNS_Number;	//
		public String Email_Address;	//
		public String Entity_Status;	//
		public String Entity_Type;	//
		public String Firm_Number;	//
		public String First_Name;	//
		public String Full_Name;	//
		public String GUID;	//
		public String Industry;	//
		public String Is_Primary_Address;	//
		public String Is_Record_Logically_Deleted;	//
		public String Is_Tax_VAT_Exempt;	//
		public String Last_Name;
		public String Last_Update_Timestamp;	//
		public String Last_Update_User;	//
		public String Local_Language;	//
		public String Middle_Name;	//
		public String Name_Suffix;	//
		public String Phone_Number;	//
		public String SAP_Number;	//
		public String SBU_Name;	//
		public String Salesforce_Account_ID;	//
		public String Source_Additional_Name_1;	//
		public String Source_Additional_Name_2;	//
		public String Source_Country_Code;	//
		public String Source_System_Customer_ID;	//
		public String Source_System_Name;	//
		public String State_Province_Code;	//
		public String Sub_SBU_Name;	//
		public String VAT_Code;	//
		public String Zip_Postal_Code;	//
	}
	
	public static AccountRequestWrapper parse(String json){
		return (AccountRequestWrapper) System.JSON.deserialize(json, AccountRequestWrapper.class);
	}
	
}
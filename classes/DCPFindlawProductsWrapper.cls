/********************************************************************************
Class Name : DCPRenewalFindlawWrapper
@description : Request Structure for Findlaw Array
********************************************************************************
CHANGE HISTORY
===============================================================================
Date                         Developer              Description
25-07-2021                  Herin Acharya          Initial Creation
21-10-2021                  Vishnu Santhosh        DCP-5870 | Added 'chargeType' and 'priceType'
01-11-2021                  Vishnu Santhosh        DCP-6071 | Added 'optionProducts'
*/ 
public class DCPFindlawProductsWrapper extends DCPRenewalProductWrapper{
    public decimal price;
    public string materialNumber;
    public List<FindlawSubProducts> listSubProducts;
    public List<String> contractNumbers;
    public String chargeType;
    public String priceType;
    public List<String> optionProducts;

    /**
    @description Constructor
    */
    public DCPFindlawProductsWrapper(){
        listSubProducts = new List<FindlawSubProducts>();
    }
    /**
    * @description Class for the Address details components.
    */
    public class FindlawSubProducts{
        public String lineItemId;
        public String practiceArea;
        public String geography;
    } 
}
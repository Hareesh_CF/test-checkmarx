/********************************************************************************************************
* @description DCPAddOnRuleHandler 
* Purpose: To create the Addon rules 
* Created: 18-Oct-2021
*********************************************************************************************************/
public with sharing class DCPAddOnRuleHandler {
    /********************************************************************************************************
    *  @description detailRecId : getter setter 
    *********************************************************************************************************/ 
     public String detailRecId {set; get; }
     /********************************************************************************************************
    *  @description returnURL : getter setter 
    *********************************************************************************************************/ 
     public String returnURL {set; get; }
      /********************************************************************************************************
    *  @description DCPAddOnRuleHandler : class constuctor 
    *  @param  controller - standard controller
    *********************************************************************************************************/  
    public DCPAddOnRuleHandler(ApexPages.StandardController controller){
        this.detailRecId = controller.getId();
        this.returnURL = ApexPages.currentPage().getParameters().get('retURL');
    }
     /********************************************************************************************************
    *  @description setRulesData : Method to set rule data
    *  @param wrapper - expected string 
    *********************************************************************************************************/ 
    @AuraEnabled
    public static void setRulesData(String wrapper) {
        Wrapper wrap = (Wrapper)JSON.deserialize(wrapper, Wrapper.class);
        List<Product_Detail__c> prodDetLst;
        if(Schema.sObjectType.Product_Detail__c.isAccessible()){
            prodDetLst = [Select Id from Product_Detail__c where DCP_Store__c =: wrap.StoreId and Group_Name__c =: wrap.groupName];
        }
        if(!prodDetLst.isEmpty()){
            wrap.recordId = prodDetLst[0].Id;
        }
        
        Product_Detail__c prodDet = new Product_Detail__c(Id=wrap.recordId, Group_Name__c = wrap.groupName, DCP_AddonRuleSet__c = wrap.ruleJSON, DCP_Store__c = wrap.StoreId );
        if(Schema.sObjectType.Product_Detail__c.isUpdateable() && Schema.sObjectType.Product_Detail__c.isCreateable()){
            upsert prodDet;
        }
    }
    /********************************************************************************************************
    *  @description getGroupPickList : Method to get goups list in pickist
    *  @return Map<String, String>: map of string  
    *  @param recordId - expected product detail id
    *  @param storeId - expected store id 
    *********************************************************************************************************/    
    @AuraEnabled 
    public static Map<String, String> getGroupPickList(String recordId, String storeId){
        Map<String,String> options = new Map<String, String>();
        List<Product_Detail__c> prodDet = new List<Product_Detail__c>();
        if(String.isBlank(storeId) && Schema.sObjectType.Product_Detail__c.isAccessible()){
            prodDet = [Select DCP_Store__c from Product_Detail__c where Id =:recordId];
        }
        if(!prodDet.isEmpty()){
           storeId = prodDet[0].DCP_Store__c;
        }
        Set<String> grpSet = new Set<String>();
        if(Schema.sObjectType.APTSECOM_Product_Recommendation__c.isAccessible()){
            for(APTSECOM_Product_Recommendation__c rec: [Select Group_Name__c from APTSECOM_Product_Recommendation__c where APTSECOM_Store__c=:storeId]){
                if(!grpSet.contains(rec.Group_Name__c)){
                    options.put(rec.Group_Name__c,rec.Group_Name__c);
                    grpSet.add(rec.Group_Name__c);
                }            
            }
        }
        return options;
    }
     /********************************************************************************************************
    *  @description getStorePickList : Method to get stores list in pickist
    *  @return Map<String, String>: map of string  
    *********************************************************************************************************/       
    @AuraEnabled 
    public static Map<String, String> getStorePickList(){
        Map<String,String> options = new Map<String, String>();
        if(Schema.sObjectType.APTSECOM_Store__c.isAccessible()){
            for(APTSECOM_Store__c store : [Select Name, Id from APTSECOM_Store__c]){
                options.put(store.Id,store.Name);
            }
        }
        return options;
    }
   /********************************************************************************************************
    *  @description getRulesData : Method to get rules data
    *  @return wrapper: return wrapper class  
    *  @param recordId - expected product detail id
    *********************************************************************************************************/   
    @AuraEnabled 
    public static DCPAddOnRuleHandler.Wrapper getRulesData(String recordId){
        List<Product_Detail__c> detailList;
        if(String.isBlank(recordId)){
            return new Wrapper();
        }
        if(Schema.sObjectType.Product_Detail__c.isAccessible()){
            detailList= [Select Id, Group_Name__c , DCP_AddonRuleSet__c , DCP_Store__c from Product_Detail__c where ID = :recordId ];
        }
            Wrapper wrap = new Wrapper();
        if(!detailList.isEmpty()){
            wrap.storeId = detailList[0].DCP_Store__c;
            wrap.recordId = recordId;
            wrap.groupName = detailList[0].Group_Name__c;
            wrap.ruleJSON = detailList[0].DCP_AddonRuleSet__c;
        }
        return wrap;
    }
    /** 
     * @description : This is the class to handle the product detail info
     */    
    
    public class Wrapper{
        /********************************************************************************************************
        *  @description storeId : getter Setter
        *********************************************************************************************************/
        @AuraEnabled public String storeId {get;set;}
        /********************************************************************************************************
        *  @description recordId : getter Setter
        *********************************************************************************************************/
        @AuraEnabled public String recordId {get;set;}
        /********************************************************************************************************
        *  @description groupName : getter Setter
        *********************************************************************************************************/
        @AuraEnabled public String groupName {get;set;}
        /********************************************************************************************************
        *  @description ruleJSON : getter Setter
        *********************************************************************************************************/
        @AuraEnabled public String ruleJSON {get;set;}
        
        /********************************************************************************************************
        *  @description Wrapper : class constuctor
        *********************************************************************************************************/
        public Wrapper(){
            this.storeId = '';
            this.recordId = '';
            this.groupName = '';
            this.ruleJSON = '';
        }
    }
}
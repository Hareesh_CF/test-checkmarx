/* Class: DCPAddonRuleHandlerTest
* Purpose: Test class for DCPAddonRuleHandler
* Created: 10/21/2021
*/
@IsTest
public class DCPAddonRuleHandlerTest {
    
    private static String storeId;
    private static List<APTSECOM_Store__c> storeList;
    private static List<Product_Detail__c> proddetList;
    
    /** 
* @description : initializing the properties
*/
    static{
        loadData();
    }
    
    /** 
* @description : Prepare test data
*/
    @TestSetup private static void setup(){
        
        LIST < Static_Values__c > staticCSList = new LIST < Static_Values__c > {
            new Static_Values__c(name = 'NewSaleSellableFlag', value__c = 'Display and Sell'),  new Static_Values__c(name = 'OrderProcessSupportEmail', value__c = 'APP-CX-DIGITAL@thomson.com')
                
                };
                    insert staticCSList;
        
        Product2 prod1 = DCPNewSaleUtilityCls.createProduct('42076533');
        Insert prod1;
        
        Product2 prod2 = DCPNewSaleUtilityCls.createProduct('42004986');
        Insert prod2;
        
        Product2 prod3 = DCPNewSaleUtilityCls.createProduct('42076533');
        Insert prod3;
        
        Product2 prod4 = DCPNewSaleUtilityCls.createProduct('42004986');
        Insert prod4;
        
        APTSECOM_Store__c store = new APTSECOM_Store__c(Name='Westlaw Classic',APTSECOM_Customer_Pricing_Segment__c='Law Firm',
                                                        APTSECOM_Default_Contract_Term__c='3 Years',Type__c='New Sales',
                                                        APTSECOM_Max_No_Of_Attorneys__c=5);
        insert store;
        
        APTSECOM_Product_Recommendation__c recommend1 = new APTSECOM_Product_Recommendation__c(
            APTSECOM_Store__c =store.Id,
            Group_Name__c = 'Single State',
            APTSECOM_Product__c = prod1.Id
        );
        insert recommend1;
        APTSECOM_Product_Recommendation__c recommend2 = new APTSECOM_Product_Recommendation__c(
            APTSECOM_Store__c =store.Id,
            Group_Name__c = 'All State',
            APTSECOM_Product__c = prod2.Id
        );
        insert recommend2;
        APTSECOM_Product_Recommendation__c recommend3 = new APTSECOM_Product_Recommendation__c(
            APTSECOM_Store__c =store.Id,
            Group_Name__c = 'National Analytical',
            APTSECOM_Product__c = prod3.Id
        );
        insert recommend3;
        APTSECOM_Product_Recommendation__c recommend4 = new APTSECOM_Product_Recommendation__c(
            APTSECOM_Store__c =store.Id,
            Group_Name__c = 'Premium Analytical',
            APTSECOM_Product__c = prod4.Id
        );
        insert recommend4;
        
        Product_Detail__c prodDet = new Product_Detail__c(Group_Name__c = 'Single State', DCP_AddonRuleSet__c = '(All State) OR (National Analytical)', DCP_Store__c = Store.Id );
        insert prodDet;
        
    }
    
    /** 
* @description : Testing the logic
*/
    @IsTest public static void testYoY(){
        Test.startTest();
        DCPAddOnRuleHandler.getGroupPickList(proddetList[0].Id,'');
        DCPAddOnRuleHandler.getStorePickList();
        DCPAddOnRuleHandler.getRulesData(proddetList[0].Id);
        DCPAddOnRuleHandler.setRulesData('{}');
        Product_Detail__c prodDet = new Product_Detail__c(Group_Name__c = 'Single State', DCP_AddonRuleSet__c = '(All State) OR (National Analytical)', DCP_Store__c = storeList[0].Id );
        
        
        DCPAddOnRuleHandler handler;
        handler = new DCPAddOnRuleHandler(new ApexPages.StandardController(prodDet));
        system.assert(prodDet.Group_Name__c!=null,'Single State');
        Test.stopTest();
    }
    
    
    /** 
* @description : Fetcing the data for class properties
*/
    private static void loadData(){        
        if(storeList == null){
            storeList = [Select Id from APTSECOM_Store__c];
        }
        if(proddetList == null){
            proddetList = [Select Id from Product_Detail__c];
        }
        
    }
    
}
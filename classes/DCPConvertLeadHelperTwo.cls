/**
* @description
* Class: DCPCovertLeadHelperTwo
* Purpose: Serves to store the details for lead conversion
* Created: 06/14/2021
* Author: Kunal Mishra
* 
* CHANGE HISTORY
* =============================================================================
* Date         Name             JIRA        Description
* 08/30/2021   Kunal M         DCP-4147    Created
* =============================================================================
**/
@SuppressWarnings('PMD.ExcessivePublicCount')
public with sharing class DCPConvertLeadHelperTwo{

/****************************************************
 * @description
 Wrapper class to store details for lead conversion
 ***************************************************/
public class ConvertLeadWrapperDetails{

    public string leadId;
    public string cartId;
    public string emailId;  
    public string quoteId ; 
    public Map<string,decimal> leadToContractTermMap;
    public String accNumber;//Added as part of DCP-4355
    public String accountId;//Added as part of DCP-4355
    public Source_System_Detail__c ssdRec;//Added as part of DCP-4355
    public String trialID;//Added as part of DCP-4147
    public string trialStatus;//Added as part of DCP-4147
    public boolean reviewRequired;//Added as part of DCP-4147 & DCP-4231
    public string reviewRequiredNotes;//Added as part of DCP-4147
    public string onePassGUID;
 }
 
     /****************************************************
 * @description
 wrapper class to store details for process trial conversion
 ***************************************************/
 public class ProcessTRStoreWrapperDetails{
    /**@return 
     * @description 
     * @param 
    */
    public string quoteId{get;set;}
    /**@description
     * @return
     * @param
     */
     public string cartId{get;set;}
     /**@description
     * @return
     * @param
     */
     public string leadId{get;set;}
     /**@description
     * @return
     * @param
     */
     public string trialId{get;set;}
     /**@return 
     * @description 
     * @param 
    */
      
   public boolean reviewRequired{get;set;} //Added by priyanka for dcp-4231
     /**@return 
     * @description 
     * @param 
    */
      
    public string reviewRequiredNotes{get;set;} //Added by priyanka for dcp-4231
         /**@return 
     * @description 
     * @param 
    */
    public string onePassGUID{get;set;}
             /**@return 
     * @description 
     * @param 
    */
    public string orderOriginSite{get;set;}

 }
 
     /********************************************************************************************************
* @description getOrgTypeSegment : Method to populate Segment Value
* @param segmentVal - 
* @return returnVal

*********************************************************************************************************/
    public static String getOrgTypeSegment(String segmentVal){
        String returnVal;
        if(String.isBlank(segmentVal)){
            returnVal = segmentVal;
        }
        else if(segmentVal.contains('Corporate')){
            returnVal = 'Corporate';
        }
        else if(segmentVal.contains('Small Law')){
            returnVal = 'Law Firm';
        }
        else if(segmentVal.contains('Government')){
            returnVal = 'Government';
        }
        return returnVal;
    }
         /********************************************************************************************************
* @description mapIndustryCode : Method to populate Segment Value
* @param orgTypeSegment - 
* @return industryCode

*********************************************************************************************************/
    public static String mapIndustryCode(String orgTypeSegment){
        String industryCode;
        if(String.isBlank(orgTypeSegment)){
            industryCode = orgTypeSegment;
        }
        else if(orgTypeSegment.toUpperCase().contains('FIRM-SOLO PRACTIONER')){
            industryCode = 'FS';
        }
        else if(orgTypeSegment.toUpperCase().contains('FIRM-LAW SMALL')){
            industryCode = 'FLS';
        }
        else if(orgTypeSegment.toUpperCase().contains('FIRM-LAW MEDIUM')){
            industryCode = 'FLM';
        }
        else if(orgTypeSegment.toUpperCase().contains('FIRM-LAW LARGE')){
            industryCode = 'FLL';
        }
        return industryCode;
    }
             /********************************************************************************************************
* @description getProductName : Method to populate Segment Value
* @param prodName - 
* @return productName

*********************************************************************************************************/
    public static String getProductName(String prodName){
        String productName;
        productName = prodName.length()>80 ? prodName.substring(0,80):prodName;
        return productName;
    }
}
/* Class: DCPNewSalesAddOnRequestHelper
* Purpose: A helper class to handle the business logic class
* Created: 9/3/2021
**/
/**
* @description A helper class for business logic
*
* CHANGE HISTORY
* =============================================================================
* Date         Name             JIRA        Description
* 10/29/2021   Pawan            DCP-5702     Updated attorneys list in getPrice().
*                                            Created getMax_No_Of_Attorneys method
* =============================================================================
*/
public with sharing class DCPNewSalesAddOnRequestHelper {

    static DCPNewSalesYoYUtil yoyUtil;
    static DCPNewSalesDataUtil.ContractTermPlanWrapper termWrapper;
    static Product2 prod;
    private static String serviceVersion {
        get{
            if(serviceVersion == null){
                serviceVersion = Static_Values__c.getValues('DCPNewSalesAddONServiceVersion')<> null ? Static_Values__c.getValues('DCPNewSalesAddONServiceVersion').Value__c : 'v1';
            }
            return serviceVersion;
        }
        set;
    } 

    private static String queryVersion {
        get{
            if(queryVersion == null){
                queryVersion = Static_Values__c.getValues('DCPAddonQueryFilter')<> null ? Static_Values__c.getValues('DCPAddonQueryFilter').Value__c : 'v1';
            }
            return queryVersion;
        }
        set;
    }  

    /********************************************************************************************************
    *  @description  getAddonProducts - Method to get the Addons for the product/plan
    *  @return Map<String,product2>  : this is a return statement and having return list of addon products 
    *  @param requestParams - expected the request parameters 
    *********************************************************************************************************/
        public static Map<String,product2> getAddonProducts (Map<String,String> requestParams){
            String prodCode = requestParams.get('productCode');
            String segment = requestParams.get('segment');
            
            String validSellableStatus = Static_Values__c.getValues('NewSaleSellableFlag').Value__c;
            String filterVar = Static_Values__c.getValues('DCPAddonQueryFilterField').Value__c;
            List<String> sellableValuesList = validSellableStatus.split(',');
            system.debug(LoggingLevel.INFO,'Print:'+prodCode+segment+validSellableStatus+sellableValuesList);
            Map<string,product2> addOnList = new Map<String,product2>();
            String query = 'Select Product_To__c,Product_To__r.Name,Product_To__r.DC_Short_Description__c,Product_To__r.productcode,Product_To__r.Apttus_Filter_Jurisdiction__c,Product_To__r.CPD_Sellable_Description__c,Product_To__r.Product_Offering__c,Product_From__r.productcode,Product_From__r.product_offering__c,Product_From__c,Relationship_Type__c,Relationship_Valid_From__c,Relationship_Valid_To__c, Relationship_Link_Type__c, Segment__c, Sub_Segment__c from Product_Relationship__c where (product_from__r.productCode =:prodCode and segment__c=:segment and Product_To__r.CPD_Sellable_Description__c IN:sellableValuesList and Relationship_Type__c = \'' + String.escapeSingleQuotes('Accessory Product') + '\')';
            if(queryVersion=='v2' && String.isNotBlank(filterVar)){
                DateTime currentTime = System.now();
                system.debug(LoggingLevel.INFO,'Print:'+currentTime);
                query += ' and ('+filterVar+' >= :currentTime OR '+filterVar+' = null )';
            }
                for(Product_Relationship__c prodRel:System.Database.query(query)){
                prod = new Product2(Id=prodRel.product_from__c, productCode = prodRel.product_from__r.productCode,product_offering__c=prodRel.product_from__r.product_offering__c);       
                addOnList.put(prodRel.Product_To__r.productCode,new product2(Name=prodRel.product_To__r.Name,Id=prodRel.product_To__c,DC_Short_Description__c=prodRel.Product_To__r.DC_Short_Description__c,productCode=prodRel.Product_To__r.productcode,CPD_Sellable_Description__c=prodRel.Product_To__r.CPD_Sellable_Description__c,Product_Offering__c=prodRel.Product_To__r.Product_Offering__c,Apttus_Filter_Jurisdiction__c=prodRel.Product_To__r.Apttus_Filter_Jurisdiction__c));
            }
            return addOnList;
        }
        
          /********************************************************************************************************
    *  @description  getPrice - This is the main method to handle the pricing Array  
    *  @return Map<String,list<Apttus_Config2__PriceMatrixEntry__c>>  : this is a return statement and having the price matrix Array
    *  @param productCodes - expected the product parameters 
    *  @param segment - expected the segment parameters 
    *********************************************************************************************************/
        
        public static Map<String,List<Apttus_Config2__PriceMatrixEntry__c>> getPrice (Map<string,product2> productCodes, String segment){
            Map<String,List<Apttus_Config2__PriceMatrixEntry__c>> quantityPriceMatrixMap = new Map<String,List<Apttus_Config2__PriceMatrixEntry__c>>();
            Map<Decimal, Apttus_Config2__PriceMatrixEntry__c> mapPriceMatrixEntry = new Map<Decimal, Apttus_Config2__PriceMatrixEntry__c>();
            Set<String> prodCodes =  productCodes.keyset();
            system.debug(loggingLevel.INFO,'ProductCodes >>'+prodCodes);
            Set < String > attorneys = new Set < String > ();
            
            //Added by Pawan Start as Part of DCP-5702
            Integer maxNoOfAttorneys = getMaxNoOfAttorneys(segment);
            for (Integer i = 1; i <= maxNoOfAttorneys ; i++) {
                attorneys.add(String.valueOf(i));
            }
            //Added by Pawan End
            
            
            // Commented By Pawan
            /*for (Integer i = 1; i <= 25; i++) {
                attorneys.add(String.valueOf(i));
            }*/
            
            
            
            
            string query = 'SELECT Id,Apttus_Config2__Sequence__c,Apttus_Config2__PriceMatrixId__r.Apttus_Config2__PriceListItemId__c,Apttus_Config2__PriceMatrixId__r.Apttus_Config2__PriceListItemId__r.Apttus_Config2__PriceListId__c,Apttus_Config2__PriceMatrixId__r.Apttus_Config2__PriceListItemId__r.CurrencyIsoCode,Apttus_Config2__PriceMatrixId__r.Apttus_Config2__PriceListItemId__r.Apttus_Config2__Frequency__c,Apttus_Config2__PriceMatrixId__r.Apttus_Config2__PriceListItemId__r.Apttus_Config2__ProductId__r.productCode,Apttus_Config2__PriceMatrixId__r.Apttus_Config2__PriceListItemId__r.Apttus_Config2__ProductId__r.Product_Offering__c,Apttus_Config2__Dimension1Value__c, Apttus_Config2__Dimension2Value__c, Apttus_Config2__AdjustmentAmount__c,Apttus_Config2__AdjustmentType__c FROM Apttus_Config2__PriceMatrixEntry__c WHERE Apttus_Config2__PriceMatrixId__r.Apttus_Config2__PriceListItemId__r.Apttus_Config2__ProductId__r.productCode IN:prodCodes  AND Apttus_Config2__Dimension2Value__c =:segment AND Apttus_Config2__Dimension1Value__c IN:attorneys order by Id DESC ';
            Map<Decimal, Decimal> dim1SeqMap;
            dim1SeqMap = new Map<Decimal,Decimal>();
            
            
            for(Apttus_Config2__PriceMatrixEntry__c pme: System.Database.Query(query)){
            system.debug(loggingLevel.INFO,'pme>>'+pme);
            // if (pme!=null){
             system.debug(loggingLevel.INFO,'pme>>'+pme);
              String productId = pme.Apttus_Config2__PriceMatrixId__r.Apttus_Config2__PriceListItemId__r.Apttus_Config2__ProductId__r.productCode;
                if(quantityPriceMatrixMap.containsKey(productId)) {
                    List<Apttus_Config2__PriceMatrixEntry__c> pmeList = quantityPriceMatrixMap.get(productId);
                    pmelist.add(pme);
                    quantityPriceMatrixMap.put(productId,pmelist);
                } else {
                    quantityPriceMatrixMap.put(productId, new List<Apttus_Config2__PriceMatrixEntry__c> { pme });
                }
            // }
            }
            Map<String, List<Apttus_Config2__PriceMatrixEntry__c>> mapMatrixEntry = getCorrectMatrixEntries(quantityPriceMatrixMap);
            if(!mapMatrixEntry.isEmpty()){
                system.debug(loggingLevel.INFO,'mapMatrixEntry>>'+mapPriceMatrixEntry);
                return mapMatrixEntry;
            }else {
                system.debug(loggingLevel.INFO,'mapMatrixEntry>>'+mapMatrixEntry);
                return quantityPriceMatrixMap;
            }
            //return quantityPriceMatrixMap;
        }
        
      
        private static Map<String,List<Apttus_Config2__PriceMatrixEntry__c>> getCorrectMatrixEntries(Map<String,List<Apttus_Config2__PriceMatrixEntry__c>> quantityPriceMatrixMap){
           Map<String, List<Apttus_Config2__PriceMatrixEntry__c>> mapMatrixEntry = new Map<String, List<Apttus_Config2__PriceMatrixEntry__c>>();
            for(String prodId : quantityPriceMatrixMap.keySet()){
                List<Apttus_Config2__PriceMatrixEntry__c> pmList = quantityPriceMatrixMap.get(prodId);
                //List<Apttus_Config2__PriceMatrixEntry__c> entryList= new List<Apttus_Config2__PriceMatrixEntry__c>();
                //Map<Decimal, Decimal> dim1SeqMap = new Map<Decimal,Decimal>();
                fetchCorrectEntries(prodId,pmList,mapMatrixEntry);            
            }
            return mapMatrixEntry;
        }
        
        private static void fetchCorrectEntries(String prodCode, List<Apttus_Config2__PriceMatrixEntry__c> pmList, Map<String, List<Apttus_Config2__PriceMatrixEntry__c>> mapMatrixEntry){
            Map<Decimal, Apttus_Config2__PriceMatrixEntry__c> mapPriceMatrixEntry = new Map<Decimal, Apttus_Config2__PriceMatrixEntry__c>();
            Map<Decimal, Decimal> dim1SeqMap = new Map<Decimal,Decimal>();
            for(Apttus_Config2__PriceMatrixEntry__c entry:pmList){
                    Decimal key = Decimal.valueOf(entry.Apttus_Config2__Dimension1Value__c);
                Decimal value = entry.Apttus_Config2__Sequence__c;  
                    
                    if(dim1SeqMap.containsKey(key)){
                        if(dim1SeqMap.get(key) > value) {
                           dim1SeqMap.put(key,value);
                           mapPriceMatrixEntry.put(key, entry);
                        }
                    }else{
                        dim1SeqMap.put(key,Value); 
                        mapPriceMatrixEntry.put(key, entry);
                    }
                        
             }
    
        if(!mapPriceMatrixEntry.isEmpty()){
               mapMatrixEntry.put(prodCode,mapPriceMatrixEntry.values());
            }
        }
       /********************************************************************************************************
    *  @description  getProducts - This is the main method to handle the product mapping
    *  @return DCPNewSalesAddOnRequest.ProductArray  : this is a return statement and having product mapping detail
    *  @param prod : Expected the product  parameters 
    *********************************************************************************************************/
        public static DCPNewSalesAddOnRequest.ProductArray getProducts(product2 prod){
            DCPNewSalesAddOnRequest.ProductArray prodDetail = new DCPNewSalesAddOnRequest.ProductArray();
            prodDetail.productName = prod.Name;
            prodDetail.productId = prod.Id;
            prodDetail.productCode = prod.productCode;
            prodDetail.shortDescription = DCPNewSaleUtilityCls.handleEmptyString(prod.DC_Short_Description__c, DCPNewSaleUtilityCls.notAvail);
            
            return prodDetail;
        }
    
      /********************************************************************************************************
    *  @description  getPricing - This is the main method to handle the product pricing map
    *  @return List<DCPNewSalesAddOnRequest.PriceList>  : this is a return statement and having product pricing mapping detail
    *  @param prod - expected the product  parameters 
    *  @param priceMatrixMap - priceMatrixMap contains the possible prices
    *********************************************************************************************************/
        /*public static List<DCPNewSalesAddOnRequest.PriceList> getPricingWithoutYOY(product2 prod,Map<String,List<Apttus_Config2__PriceMatrixEntry__c>> priceMatrixMap){
            List<DCPNewSalesAddOnRequest.PriceList> priceDetailList = new List<DCPNewSalesAddOnRequest.PriceList>();
            if(priceMatrixMap!=null &&priceMatrixMap.get(prod.productCode)!=null){
                for(Apttus_Config2__PriceMatrixEntry__c pme:priceMatrixMap.get(prod.productCode)){
                    DCPNewSalesAddOnRequest.PriceList priceDetail = new DCPNewSalesAddOnRequest.PriceList();
                    priceDetail.quantity = pme.Apttus_Config2__Dimension1Value__c;
                    priceDetail.Amount = String.valueOf(pme.Apttus_Config2__AdjustmentAmount__c);
                    priceDetailList.add(priceDetail);
                }
            }
                return priceDetailList;
        }*/
        
    /********************************************************************************************************
    *  @description  getPLIInfo - This is the main method to handle the PLI INFO
    *  @return Map<String,Apttus_Config2__PriceListItem__c>  : this is a return statement and having product pricing mapping detail
    *  @param prodMap - expected the product  parameters 
    *********************************************************************************************************/
        public static Map<String,Apttus_Config2__PriceListItem__c> getPLIInfo(Map<String,Product2> prodMap){
            set<String> prodCodes = prodMap.Keyset();
            system.debug(LoggingLevel.INFO,'Print:'+prodCodes);
            Map<String,Apttus_Config2__PriceListItem__c> pliItems = new Map<String,Apttus_Config2__PriceListItem__c>();
            string query = 'Select id,Apttus_Config2__ProductId__r.productcode,Apttus_Config2__PriceListId__c,Apttus_Config2__BillingFrequency__c, CurrencyIsoCode from Apttus_Config2__PriceListItem__c where Apttus_Config2__ProductId__r.productcode IN:prodCodes';
            for(Apttus_Config2__PriceListItem__c pli:System.Database.Query(query)){
                pliItems.put(pli.Apttus_Config2__ProductId__r.productcode,pli);
            }
            
            return pliItems;
        }
     
    


 /********************************************************************************************************
*  @description  processYOYPricing - This is the main method to handle the YOY info
*  @return DCPProductRequestDeclaration.ProductArray  : this is a return statement and having product array
*  @param prod as product
*  @param priceMatrixMap - priceMatrix Array

*************************************************************************************/
    public static List<DCPNewSalesYoYUtil.PriceMatrix> getPricing(product2 prod,Map<String,List<Apttus_Config2__PriceMatrixEntry__c>> priceMatrixMap){
        List<DCPNewSalesYoYUtil.PriceMatrix> priceMatrixList = new List<DCPNewSalesYoYUtil.PriceMatrix>();
        List<Apttus_Config2__PriceMatrixEntry__c> priceMap = new List<Apttus_Config2__PriceMatrixEntry__c>(priceMatrixMap.get(prod.productCode));
        try{
            //if(serviceVersion=='v2'){
                priceMatrixList = DCPNewSalesAddOnRequestHelper.getPriceMatrix(prod.Id,priceMap);
            //}else{
                //priceMatrixList = DCPNewSalesAddOnRequestHelper.getPricingWithoutYOY(prod,priceMap);
            //}
        }    
        catch(exception ex){
            DCPNewSalesAddOnRequest.ProductAddonResponse resp= new DCPNewSalesAddOnRequest.ProductAddonResponse();
            resp.response = DCPNewSaleUtilityCls.responseMethod(DCPNewSaleUtilityCls.successStatusOne,DCPNewSaleUtilityCls.detailsNotFound,DCPNewSaleUtilityCls.successStatus);
            
        }
    return priceMatrixList;
        
    }

    
    /********************************************************************************************************
*  @description  getTermWrapper - This method fetches New Sales Contract Terms 
*  @return DCPNewSalesDataUtil.ContractTermPlanWrapper  : Returns the mapping of plans and NSCT terms  
*  @param storeName 
*  @param segment and 
*  @param productIds set of productIds
*********************************************************************************************************/
    public static DCPNewSalesDataUtil.ContractTermPlanWrapper getTermWrapper(String storeName, String segment, Set<Id> productIds){
        DCPNewSalesYoYUtil.YoYRequestWrapper youWrap = new DCPNewSalesYoYUtil.YoYRequestWrapper();
        youWrap.productIds = productIds;
        youWrap.storeName = storeName +' '+segment;
        //youWrap.storeName = 'Westlaw Edge'+' '+segment;
        system.debug(loggingLevel.INFO,'storeName>>>'+ youWrap.storeName);
        youWrap.storeType = 'New Sales';
        youWrap.customerSegment = segment;
       
        DCPNewSalesStoreUtility.StoreWrapper storeData = DCPNewSalesStoreUtility.getStoreDetails(youWrap.StoreName, youWrap.customerSegment, youWrap.storeType);
        return DCPNewSalesPlanUtility.getContractPlans(storeData,productIds);    
    }


  /********************************************************************************************************
*  @description  preparePriceData - Internal method to get the price data after applying discounts and YoY if avaiable 
*  @param productId - productId
*  @param entry - price matrix entry record and 
*  @param priceMatrixEntryList - list of priceMatix
*********************************************************************************************************/
    private static void preparePriceData(Id productId, Apttus_Config2__PriceMatrixEntry__c entry, List<DCPNewSalesYoYUtil.PriceMatrix> priceMatrixEntryList){
        Boolean isSuccess = false;
        Boolean isYoYRequested = serviceVersion == 'v2' || serviceVersion == 'v3';
        isSuccess = isYoYRequested ? yoyUtil.getYoyMatrixData(productId,entry,priceMatrixEntryList) : false;
        if(!isSuccess){
            DCPNewSalesYoYUtil.PriceMatrix pl = new DCPNewSalesYoYUtil.PriceMatrix();
            pl.quantity = entry.Apttus_Config2__Dimension1Value__c;
            pl.amount = String.valueOf(entry.Apttus_Config2__AdjustmentAmount__c);
            priceMatrixEntryList.add(pl);
        }
        
    }


    /********************************************************************************************************
*  @description  getPriceMatrix - This is the main method to handle the price Matrix response 
*  @return list<DCPProductRequestHandler.PriceList>  : this is a return statement and having list of price Matrix
*  @param productId as product Id
*  @param priceMap - expected the pricing parameters 

*********************************************************************************************************/
    
    private static list<DCPNewSalesYoYUtil.PriceMatrix> getPriceMatrix(Id productId, List<Apttus_Config2__PriceMatrixEntry__c> priceMap){
        list<DCPNewSalesYoYUtil.PriceMatrix> priceMatrixEntryList = new list<DCPNewSalesYoYUtil.PriceMatrix>();
        Boolean isSuccess = false;
        system.debug(loggingLevel.INFO,'Success:'+isSuccess);
        if(!priceMap.isEmpty() && priceMap.size()>0){
            for(Apttus_Config2__PriceMatrixEntry__c entry:priceMap){
                DCPNewSalesAddOnRequestHelper.preparePriceData(productId,entry,priceMatrixEntryList);
            }
        }
        return priceMatrixEntryList;
    }

        /********************************************************************************************************
*  @description  getPriceMatrix - This is the main method to handle the price Matrix response 
*  @param requestParams - get all params passed in request
*  @param productMap - get all the Addon products

*********************************************************************************************************/
    public static void processYOY(Map<String,String> requestParams,Map<String,product2> productMap){
         if(serviceVersion == 'v2' || serviceVersion == 'v3'){
            String segment = requestParams.get('segment');
            Set<Id> prodIds = new Set<Id>();
            
            String brand = (prod!=null)?prod.product_offering__c:null;
                for(product2 prod: productMap.Values()){
                    prodIds.add(prod.Id);
                }
            yoyUtil = new DCPNewSalesYoYUtil();
            yoyUtil.serviceVersion = serviceVersion;
            yoyUtil.termWrapper = getTermWrapper(brand, segment,prodIds);
            System.debug(LoggingLevel.ERROR, DCPNewSalesDataUtil.errorMessage);
        }
    }
     
    
    
/********************************************************************************************************
*  @description  getMax_No_Of_Attorneys - This Method will return the maximum no of attorneys for a segment
*  @return max_No_Of_Attorneys  : This will holds the number of attorneys for a segment
*  @param segment - expected the Segment as  parameter
*  @author - Pawan (created as part of DCP-5702)
*********************************************************************************************************/    
    public static Integer getMaxNoOfAttorneys(string segment){
       
        Integer maxNoOfAttorneys=0;
        APTSECOM_Store__c storeRec  = new APTSECOM_Store__c();
        if(schema.SObjectType.APTSECOM_Store__c.isAccessible() && schema.SObjectType.APTSECOM_Store__c.isQueryable()){
           storeRec= [SELECT Id, Name, APTSECOM_Max_No_Of_Attorneys__c, APTSECOM_Customer_Pricing_Segment__c FROM  APTSECOM_Store__c where APTSECOM_Customer_Pricing_Segment__c = :segment WITH Security_Enforced limit 1];
        }
         if(storeRec != null ){
            maxNoOfAttorneys = Integer.valueOf(storeRec.APTSECOM_Max_No_Of_Attorneys__c);
        }
        
        return maxNoOfAttorneys;
    }  
}
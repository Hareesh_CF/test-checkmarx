/**
 * @description Helper class for APTS_OrderQueueToESI    
 */
@SuppressWarnings('PMD') 
public with sharing class APTS_OrderSubmissionUtilityExt5 {
    /**
     * @description assignOnlineContactType
     * @return OnlineContactType
     * @param onlinecon1
     * @param materialBrandCodeMap
     * @param onlineContact
     */
    public static CreateOrderRequest.onlineContacts assignOnlineContactType(Online_Contacts__c onlinecon1, Map < string, string > materialBrandCodeMap, CreateOrderRequest.onlineContacts onlineContact) {

        if (onlinecon1.Type__c == 'Admin' && !materialBrandCodeMap.isEmpty() && materialBrandCodeMap.containsKey(onlinecon1.Material__c) && materialBrandCodeMap.get(onlinecon1.Material__c) == '064') {
            onlineContact.onlineContactType = '0230';
        } else if (onlinecon1.Position__c == 'Attorney') {
            onlineContact.onlineContactType = '0100';
        } else if (onlinecon1.Position__c == 'Clerk') {
            onlineContact.onlineContactType = '0050';
        } else if (onlinecon1.Position__c == 'Judge') {
            onlineContact.onlineContactType = '0080';
        } else if (onlinecon1.Position__c == 'Librarian') {
            onlineContact.onlineContactType = '0060';
        } else if (onlinecon1.Position__c == 'Non-Attorney') {
            onlineContact.onlineContactType = '0090';
        } else if (onlinecon1.Position__c == 'Paralegal') {
            onlineContact.onlineContactType = '0030';
        } else if (onlinecon1.Position__c == 'Administrator') {
            onlineContact.onlineContactType = '0290';
        } else if (onlinecon1.Position__c == 'Analyst') {
            onlineContact.onlineContactType = '0310';
        } else if (onlinecon1.Position__c == 'Supervisor') {
            onlineContact.onlineContactType = '0280';
        } else if (onlinecon1.Position__c == 'Clear Administrator') {
            onlineContact.onlineContactType = '0290';
        } else if (onlinecon1.Position__c == 'Clear Analyst') {
            onlineContact.onlineContactType = '0310';
        } else if (onlinecon1.Position__c == 'Clear Investigator') {
            onlineContact.onlineContactType = '0300';
        } else if (onlinecon1.Position__c == 'Clear Supervisor') {
            onlineContact.onlineContactType = '0280';
        } else if (onlinecon1.Position__c == 'Clear Technical') {
            onlineContact.onlineContactType = '0270';
        } else if (onlinecon1.Position__c == 'Summer Associates') {
            onlineContact.onlineContactType = '0270';
        }
        //Start:GLP-583
        else if (onlinecon1.Position__c == 'Proview Ent IP') {
            onlineContact.onlineContactType = '0900';
        }
        //End- GLP-583 

        //onlineContact.phone = null;
        //onlineContact.phoneExtension = null;
        onlineContact.contactNumber = onlinecon1.ContactID__c;

        return onlineContact;
    }
    /**
     * @description 
     * @param ordline
     * @param ordeline
     * @return ordline
     */
    public static Decimal returnNetAmount(CreateOrderRequest.orderLines ordline, Apttus_Config2__OrderLineItem__c ordeline){
        if(ordeline.Apttus_Config2__BillingFrequency__c=='Yearly' && ordeline.Apttus_Config2__BillingFrequency__c !=null){
            ordline.netAmount=ordeline.APTS_Bridge_Monthly_Charge__c!=null ? (ordeline.APTS_Bridge_Monthly_Charge__c/12).setscale(5) : null; //DOC-15134, 15135
        }
        else{
            ordline.netAmount=ordeline.APTS_Bridge_Monthly_Charge__c!=null ? ordeline.APTS_Bridge_Monthly_Charge__c.setscale(5) : null;
        }
        return ordline.netAmount;
    }
    /**
     * @description 
     * @param ordline
     * @param ordeline
     * @return ordline.listAmount
     */
    public static Decimal returnListAmount(CreateOrderRequest.orderLines ordline, Apttus_Config2__OrderLineItem__c ordeline){
        if(ordeline.Apttus_Config2__BillingFrequency__c=='Yearly' && ordeline.Apttus_Config2__BillingFrequency__c!=null){
            ordline.listAmount=ordeline.Apttus_Config2__ListPrice__c != null ? (ordeline.Apttus_Config2__ListPrice__c/12).setscale(5) : null;
        }
        else{
            ordline.listAmount=ordeline.Apttus_Config2__ListPrice__c != null ? ordeline.Apttus_Config2__ListPrice__c.setscale(5) : null;
        } 
        return ordline.listAmount;
    
    }
    public static void setBankAccountForNewSales(CreateOrderRequest ordreq, Apttus_Config2__Order__c order) {
        if( ordreq.CreateOrderRequest.ESIHeader.applicationId != 'SF08'){
            return;
        }
         if(order.Apttus_QPConfig__ProposalId__r.APTS_Payment_Option__c != 'Credit Card' || order.Apttus_QPConfig__ProposalId__r.APTS_Payment_Option__c=='Payment Express Auto EFT/Auto Charge' || order.Apttus_QPConfig__ProposalId__r.APTS_Payment_Option__c=='Autopay'){
             ordreq.CreateOrderRequest.orderHeader.eftPayment.bankAccount = order.Apttus_QPConfig__ProposalId__r.DCP_Bank_Account__c;
             ordreq.CreateOrderRequest.orderHeader.eftPayment.bankRouting = order.Apttus_QPConfig__ProposalId__r.DCP_Routing_Number__c;
             CreateOrderRequest.properties property1 = new CreateOrderRequest.properties();
             property1.key = 'countryCode';
             property1.value = order.Apttus_QPConfig__ProposalId__r.DCP_Country_Code__c;
             ordreq.CreateOrderRequest.orderHeader.eftPayment.properties = property1;
         }

        }
      
     public static void returnbaseValueOrderTerm(Apttus_Config2__OrderLineItem__c ordeline,Boolean isProflex){
        Decimal baseValueAnnual; //Start: DOC-17775 by Dominic
        Decimal baseValueAnnual1;
        Decimal baseValueAnnual2;
        Decimal baseValueAnnual3;
        Decimal baseValueAnnual4;
        Decimal baseValueAnnual5;
        if(ordeline.Apttus_Config2__BillingFrequency__c=='Yearly'){
            if(ordeline.Base_Value__c !=null){
                baseValueAnnual = (ordeline.Base_Value__c/12).setscale(2);}
                if(ordeline.APTS_Calculated_Year_1__c != null){baseValueAnnual1 = (ordeline.APTS_Calculated_Year_1__c/12).setscale(2);}      
                if(ordeline.APTS_Calculated_Year_2__c != null){baseValueAnnual2 = (ordeline.APTS_Calculated_Year_2__c/12).setscale(2);}
                if(ordeline.APTS_Calculated_Year_3__c != null){baseValueAnnual3 = (ordeline.APTS_Calculated_Year_3__c/12).setscale(2);}
                if(ordeline.APTS_Calculated_Year_4__c != null){baseValueAnnual4 = (ordeline.APTS_Calculated_Year_4__c/12).setscale(2);}
                if(ordeline.APTS_Calculated_Year_5__c != null){baseValueAnnual5 = (ordeline.APTS_Calculated_Year_5__c/12).setscale(2);}
           
        }
        else{
            baseValueAnnual = ordeline.Base_Value__c.setscale(2);
            if(ordeline.APTS_Calculated_Year_1__c != null){baseValueAnnual1 = (ordeline.APTS_Calculated_Year_1__c).setscale(2);}      
            if(ordeline.APTS_Calculated_Year_2__c != null){baseValueAnnual2 = (ordeline.APTS_Calculated_Year_2__c).setscale(2);}
            if(ordeline.APTS_Calculated_Year_3__c != null){baseValueAnnual3 = (ordeline.APTS_Calculated_Year_3__c).setscale(2);}
            if(ordeline.APTS_Calculated_Year_4__c != null){baseValueAnnual4 = (ordeline.APTS_Calculated_Year_4__c).setscale(2);}
            if(ordeline.APTS_Calculated_Year_5__c != null){baseValueAnnual5 = (ordeline.APTS_Calculated_Year_5__c).setscale(2);}
        }//End: DOC-17775
        APTS_OrderQueuetoESI.orderTerms(ordeline.APTS_Contract_Term__c, baseValueAnnual , ordeline.APTS_Years_2_Plus_Adjustment__c,ordeline.APTS_Yr_1_Renewal_Adjustment__c,ordeline.Apttus_Config2__LineStatus__c, ordeline.APTS_Term_Type__c,ordeline.APTS_Prorate_base_Value__c,baseValueAnnual1,baseValueAnnual2,baseValueAnnual3,baseValueAnnual4,baseValueAnnual5,ordeline.APTS_Is_Multi_Tiered_Pricing__c,ordeline.APTS_Increase_Yr1_Yr2__c,ordeline.APTS_Increase_Yr2_Yr3__c,ordeline.APTS_Increase_Yr3_Yr4__c,ordeline.APTS_Increase_Yr4_Yr5__c,isProflex);
                    
    
    }
	  /**
     * @description : DCP-6094::Canada and USL - Digital sales should be set up in SAP Revenue code 07
     * @param order-order
     * @param proposalOwner - user name
     * @param ordreq - order request
     * @return String - returns revenue channel
     */
    public static String getCanadaRevenuChannel(Apttus_Config2__Order__c order, User proposalOwner,CreateOrderRequest ordreq){
       
        if(order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c!=null && order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c!=''){
          ordreq.CreateOrderRequest.OrderHeader.revenueChannel=order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c;
          }
          // DCP-6094 
          else if(order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c==null && proposalOwner.Apts_Revenue_Channel__c ==null && proposalOwner.Sales_Force_Description__c!='Home Office' && ordreq.CreateOrderRequest.ESIHeader.applicationId == 'TRSTR'){
          ordreq.CreateOrderRequest.OrderHeader.revenueChannel='07';
          }
          //skg soc-3450
          else if(order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c==null && proposalOwner.Apts_Revenue_Channel__c ==null && proposalOwner.Market_Segment_Description__c=='Inside'){
          ordreq.CreateOrderRequest.OrderHeader.revenueChannel='05';
          }
           //SOC-5452
          else if(order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c==null && proposalOwner.Apts_Revenue_Channel__c ==null && proposalOwner.Sales_Force_Description__c=='Home Office' && proposalOwner.Market_Segment_Description__c == 'Full Territory'){
          ordreq.CreateOrderRequest.OrderHeader.revenueChannel='01';
          }
          else if(order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c==null && proposalOwner.Apts_Revenue_Channel__c ==null && proposalOwner.Sales_Force_Description__c=='Home Office'){
          ordreq.CreateOrderRequest.OrderHeader.revenueChannel='06';
          }
          else if(order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c==null && proposalOwner.Apts_Revenue_Channel__c ==null && proposalOwner.Sales_Force_Description__c!='Home Office'){
          ordreq.CreateOrderRequest.OrderHeader.revenueChannel='01';
          }
       return ordreq.CreateOrderRequest.OrderHeader.revenueChannel;
    }

    /**
     * @description : DCP-6094::Canada and USL - Digital sales should be set up in SAP Revenue code 07
     * @param order-order
     * @param proposalOwner - user name
     * @param ordreq - order request
     * @return String - returns revenue channel for USL
     */
    public static String getUSLRevenuChannel(Apttus_Config2__Order__c order, User proposalOwner,CreateOrderRequest ordreq){
       
        if(order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c!=null && order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c!=''){
            ordreq.CreateOrderRequest.OrderHeader.revenueChannel=order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c;
            }
            //DCP-6094
            else if(order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c==null && proposalOwner.Apts_Revenue_Channel__c ==null && proposalOwner.Sales_Force_Description__c!='Home Office' && ordreq.CreateOrderRequest.ESIHeader.applicationId == 'SF08'){
            ordreq.CreateOrderRequest.OrderHeader.revenueChannel='07';
            }
            //skg soc-3450
            else if(order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c==null && proposalOwner.Apts_Revenue_Channel__c ==null && proposalOwner.Market_Segment_Description__c=='Inside'){
            ordreq.CreateOrderRequest.OrderHeader.revenueChannel='05';
            }
             //SOC-5452
            else if(order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c==null && proposalOwner.Apts_Revenue_Channel__c ==null && proposalOwner.Sales_Force_Description__c=='Home Office' && proposalOwner.Market_Segment_Description__c == 'Full Territory'){
            ordreq.CreateOrderRequest.OrderHeader.revenueChannel='01';
            }
            else if(order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c==null && proposalOwner.Apts_Revenue_Channel__c ==null && proposalOwner.Sales_Force_Description__c=='Home Office'){
            ordreq.CreateOrderRequest.OrderHeader.revenueChannel='06';
            }
            else if(order.Apttus_QPConfig__ProposalId__r.APTS_Revenue_Channel_Override__c==null && proposalOwner.Apts_Revenue_Channel__c ==null && proposalOwner.Sales_Force_Description__c!='Home Office'){
            ordreq.CreateOrderRequest.OrderHeader.revenueChannel='01';
            }
       return ordreq.CreateOrderRequest.OrderHeader.revenueChannel;
    }

}
/**
*@Comment : Test class for AccountRequestWrapper for ECM
*@description : Test class for AccountRequestWrapper for ECM
*/
@istest
public class AccountRequestWrapperTest {

    /**
    *@Comment : This apex class is test for AccountRequestWrapper
    *@description : This apex class is test for AccountRequestWrapper
    */
    public static testMethod void testAccountRequest() {
      
        String jsonRequest = '{"updateCustomerRequest":{"data":[{"Zip_Postal_Code":"34221","VAT_Code":null,"Sub_SBU_Name":"SFDCGLI","State_Province_Code":"FL","Source_System_Name":"LGLSFDCGLI","Source_System_Customer_ID":"0012g00000bxr3TAAQ","Source_Country_Code":"US","Source_Additional_Name_2":null,"Source_Additional_Name_1":null,"SBU_Name":"TR ENTERPRISE","SAP_Number":null,"Salesforce_Account_ID":"0012g00000bxr3TAAQ","Phone_Number":null,"Name_Suffix":"","Middle_Name":"","Local_Language":null,"Last_Update_User":null,"Last_Update_Timestamp":"2021-09-21 08:08:28","Last_Name":"","Is_Tax_VAT_Exempt":"","Is_Record_Logically_Deleted":"0","Is_Primary_Address":"1","Industry":"","GUID":"","Full_Name":"CHECK 6 - ACCOUNT 6","First_Name":null,"Firm_Number":null,"Entity_Type":"O","Entity_Status":"ACTIVE","Email_Address":null,"DUNS_Number":"","Customer_Type":"Prospect","Customer_Number":"","Customer_Group":"","County_Name":"","City_Name":"Palmetto","BU_Classification_3":"","BU_Classification_2":"","BU_Classification_1":"Corporate","Address_Type":"OTHER","Address_Line_4":"","Address_Line_3":"","Address_Line_2":"","Address_Line_1":"1604 14th Street West","AccountId":"0012g00000bxr3TAAQ","Account_Currency":"USD"}]}}';
        Test.startTest();
        AccountRequestWrapper objReq = AccountRequestWrapper.parse(jsonRequest);
        System.assertNotEquals(null, objReq,'objReq is not null');
        Test.stopTest();
    }
}
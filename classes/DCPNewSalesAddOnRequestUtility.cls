/* Class: DCPNewSalesAddOnRequestUtility
* Purpose: An utility class for Addon Services
* Created: 10/22/2021
**/
/**
* @description An utility class
*/
public with sharing class DCPNewSalesAddOnRequestUtility {

    /********************************************************************************************************
 *  @description  getProductDetails - This is the main method to handle the product details for the addon rules 
 *  @return Map<String,product_detail__c>  : this is a return statement and having the price matrix Array
 *  @param productCodes - expected the product parameters 
 *  @param storeName - expected the StoreName parameters 
 *********************************************************************************************************/
     
     private static Map<String,product_detail__c> getProductDetails (Map<string,String> productCodes, String storeName){
         Map<String,product_detail__c> ruleLists = new Map<String,product_detail__c>();
         if(!productCodes.isEmpty() && String.isNotBlank(storeName)){
             List<String> grpNames = productCodes.values();
             //grpNames.remove(null);
             List<product_detail__c> prodDetailList = new List<product_detail__c>();
             if (Schema.sObjectType.product_detail__c.isQueryable()){
                 prodDetailList = [Select id,DCP_AddonRuleSet__c, DCP_Store__r.Name, Group_Name__c from product_detail__c where (Group_Name__c IN:grpNames AND DCP_Store__r.Name =:storeName)];
             }
             if(!prodDetailList.isEmpty()){
                 for(product_detail__c prodDet:prodDetailList){
                     ruleLists.put(prodDet.Group_Name__c,prodDet);
                 }
             }
         }
         return ruleLists;
     }

    /********************************************************************************************************
*  @description  getGrpNames - This is the main method to get the product grp name  
*  @return Map<String,String>  : this is a return statement and having return result as failure or success  
*  @param addonProd - expected the request parameters gives all addon products
*  @param brand - brand information
*  @param segment - segment information
*********************************************************************************************************/

public static Map<String,product_detail__c> getGrpNames(Map<String,product2> addonProd,string brand, string segment){
     Map<String,product_detail__c> ruleLists = new Map<String,product_detail__c>();
     Map<String,product_detail__c> productRuleLists = new Map<String,product_detail__c>();
     Set<String> addonProdCodes = addonProd.keyset();
     String validSellableStatus = Static_Values__c.getValues('NewSaleSellableFlag').Value__c;
     List<String> sellableValuesList = new List<String>();
     sellableValuesList = validSellableStatus.split(',');
         //Add-on changes - Storename
       String storeName = brand + ' ' +segment;
       Map<String,String> productGrpNames = new  Map<String,String>();
         List<APTSECOM_Product_Recommendation__c> recommendationList = new List<APTSECOM_Product_Recommendation__c>();
       if (Schema.sObjectType.APTSECOM_Product_Recommendation__c.isQueryable()){
       
         recommendationList = [SELECT APTSECOM_Product__c,
                               APTSECOM_Product__r.productcode,APTSECOM_Product__r.Name,APTSECOM_Product__r.Product_Offering__c,Group_Name__c
                               FROM APTSECOM_Product_Recommendation__c
                               WHERE APTSECOM_Product__r.ProductCode IN:AddonProdCodes and APTSECOM_Product__r.CPD_Sellable_Description__c IN:sellableValuesList and APTSECOM_Store__r.name=:storeName];
        
     }
         //Add-on change
           if(!recommendationList.isEmpty()){
             for(APTSECOM_Product_Recommendation__c  recommObject : recommendationList){
                 if(recommObject.group_Name__c!=null){
                    productGrpNames.put(recommObject.APTSECOM_Product__r.productcode,recommObject.group_Name__c);
                 }
             }
           }
        ruleLists = DCPNewSalesAddOnRequestUtility.getProductDetails(productGrpNames,storeName);
       if(!ruleLists.isEmpty()){
           for(String prodCode:productGrpNames.Keyset()){
               productRuleLists.put(prodCode,ruleLists.get(productGrpNames.get(prodCode)));
           }
       }

        return productRuleLists;
         
  }


  
    /********************************************************************************************************
*  @description  getRecmGrpNames - This is the main method to get the product grp name  
*  @return Map<String,String>  : this is a return statement and having return result as failure or success  
*  @param addonProd - expected the request parameters gives all addon products
*  @param brand - brand information
*  @param segment - segment information
*********************************************************************************************************/

public static Map<String,String> getRecmGrpNames(Map<String,product2> addonProd,string brand, string segment){

     Set<String> addonProdCodes = addonProd.keyset();
     Map<String,String> addOnRecGrpName = new Map<String,String>() ;
     String validSellableStatus = Static_Values__c.getValues('NewSaleSellableFlag').Value__c;
     List<String> sellableValuesList = new List<String>();
     sellableValuesList = validSellableStatus.split(',');
         //Add-on changes - Storename
       String storeName = brand + ' ' +segment;
      // Map<String,String> productGrpNames = new  Map<String,String>();
         List<APTSECOM_Product_Recommendation__c> recommendationList = new List<APTSECOM_Product_Recommendation__c>();
          if (Schema.sObjectType.APTSECOM_Product_Recommendation__c.isQueryable()){
       
         recommendationList = [SELECT APTSECOM_Product__c,
                               APTSECOM_Product__r.productcode,APTSECOM_Product__r.Name,APTSECOM_Product__r.Product_Offering__c,Group_Name__c
                               FROM APTSECOM_Product_Recommendation__c
                               WHERE APTSECOM_Product__r.ProductCode IN:AddonProdCodes and APTSECOM_Product__r.CPD_Sellable_Description__c IN:sellableValuesList and APTSECOM_Store__r.name=:storeName];
        
     }
     
     if(!recommendationList.isEmpty()){
             for(APTSECOM_Product_Recommendation__c  recommObject : recommendationList){
                 if(recommObject.group_Name__c!=null){
                    addOnRecGrpName.put(recommObject.APTSECOM_Product__r.productcode,recommObject.Group_Name__c);
                   
                 }
             }
           }
         return addOnRecGrpName;
}


 /********************************************************************************************************
*  @description getRecmGroupMap - Mapping of grpname to addon rule
*  @param prod  : product2 record  
*  @param recmGrpMap - expected the request parameters gives all addon product rules
*  @param prodRule - Contains mapped fields
*  @return DCPNewSalesAddOnRequest.productRule - return the rule
*********************************************************************************************************/

 public static DCPNewSalesAddOnRequest.ProductRule getRecmGroupMap(product2 prod,Map<String,String> recmGrpMap,DCPNewSalesAddOnRequest.ProductRule prodRule){
    
       prodRule.groupName = recmGrpMap.get(prod.productCode);
       System.debug(LoggingLevel.DEBUG, 'prodRule.groupName'+prodRule.groupName);
       return prodRule;
       
 }

       /********************************************************************************************************
*  @description  createRule - formation of addon rule happens here 
*  @param prod  : product2 record  
*  @param prodDetailsMap - expected the request parameters gives all addon product rules
*  @return DCPNewSalesAddOnRequest.productRule - return the rule
*********************************************************************************************************/

 public static DCPNewSalesAddOnRequest.ProductRule createRule(product2 prod,Map<String,product_detail__c> prodDetailsMap){
     DCPNewSalesAddOnRequest.ProductRule prodRule = new DCPNewSalesAddOnRequest.ProductRule();
     prodRule.productName = prod.Name;
     System.debug(LoggingLevel.DEBUG,'prodRule.productName>>>'+prodRule.productName);
    //prodRule.groupName = (prodDetailsMap.get(prod.productCode)!=null && prodDetailsMap.get(prod.productCode).group_Name__c!=null)?prodDetailsMap.get(prod.productCode).group_Name__c:null;
     prodRule.rule = (prodDetailsMap.get(prod.productCode)!=null && prodDetailsMap.get(prod.productCode).DCP_AddonRuleSet__c!=null)?prodDetailsMap.get(prod.productCode).DCP_AddonRuleSet__c: null;
     return prodRule;
 }
}
/* Class: DCPNewSalesAddOnRequestHandler
* Purpose: A handler class to handle the request between request class and the business logic class
* Created: 9/3/2021
**/
/**
* @description A handler class to handle the addonrequest
*
* CHANGE HISTORY
* ==================================================================================================================
* Date         Name             JIRA        Description
* 10/29/2021   Pawan            DCP-5702     Updated handleRequest method to add maxAttorneys attribute to response
* ===================================================================================================================
*/
public with sharing class DCPNewSalesAddOnRequestHandler {

    public static Map<String,String> reqParamsMap;
    public static Map<String,product_detail__c> addonRules;
    public static Map<String,String> addonGroup;

    public Static DCPNewSalesAddOnRequest.ProductAddOnResponse resp;
    private static String serviceVersion {
        get{
            if(serviceVersion == null){
                serviceVersion = Static_Values__c.getValues('DCPNewSalesAddONServiceVersion')<> null ? Static_Values__c.getValues('DCPNewSalesAddONServiceVersion').Value__c : 'v1';
            }
            return serviceVersion;
        }
        set;
    } 

    private static String addonRuleEnabler {
        get{
            if(addonRuleEnabler == null){
                addonRuleEnabler = Static_Values__c.getValues('DCPAddonRuleEnabler')<> null ? Static_Values__c.getValues('DCPAddonRuleEnabler').Value__c : 'v1';
            }
            return addonRuleEnabler;
        }
        set;
    } 
    /********************************************************************************************************
    *  @description  handleRequest - This is the main method to handle the request  
    *  @return DCPNewSalesAddOnRequest.ProductPlanResponse  : this is a return statement and having return result as failure or success  
    *  @param requestBody - expected the request parameters 
    *********************************************************************************************************/
        
        public static DCPNewSalesAddOnRequest.ProductAddOnResponse handleRequest (Map<String,String> requestBody){
            Boolean hasSegment = false;
            resp = new DCPNewSalesAddOnRequest.ProductAddOnResponse();
            Map<String,product2> productMap = new Map<String,product2>();
            Map<String,List<Apttus_Config2__PriceMatrixEntry__c>> priceMatrixMap = new Map<String,List<Apttus_Config2__PriceMatrixEntry__c>>();
            if((requestBody.containskey('segment') && !String.isBlank(requestBody.get('segment'))) && (requestBody.containskey('productCode') && !String.isBlank(requestBody.get('productCode')))){ 
                productMap = DCPNewSalesAddOnRequestHelper.getAddonProducts(requestBody);
                reqParamsMap = requestBody;
                hasSegment =true;
            }else{
                resp.response =  DCPNewSaleUtilityCls.responseMethod(DCPNewSaleUtilityCls.errorCode,DCPNewSaleUtilityCls.requiredFieldMissing,DCPNewSaleUtilityCls.failureStatus);
            }
    
            if(!productMap.isEmpty()){
                if(addonRuleEnabler == 'v2'&& requestBody.containsKey('brand') && !String.isBlank(requestBody.get('brand'))){
                    addonRules = DCPNewSalesAddOnRequestUtility.getGrpNames(productMap, requestBody.get('brand'), requestBody.get('segment'));
                    addonGroup = DCPNewSalesAddOnRequestUtility.getRecmGrpNames(productMap, requestBody.get('brand'), requestBody.get('segment'));
                }
                priceMatrixMap = DCPNewSalesAddOnRequestHelper.getPrice(productMap,requestBody.get('segment'));
                resp.response = DCPNewSaleUtilityCls.responseMethod(DCPNewSaleUtilityCls.successCode,DCPNewSaleUtilityCls.detailsFound,DCPNewSaleUtilityCls.successStatus);
                resp = DCPNewSalesAddOnRequestHandler.getProductResponse(productMap,priceMatrixMap);
                //Added By Pawan as part of DCP-5702 
                resp.response.maxAttorneys = string.valueOf(DCPNewSalesAddOnRequestHelper.getMaxNoOfAttorneys(requestBody.get('segment')));
        
            }else{
                resp.response = DCPNewSaleUtilityCls.responseMethod(DCPNewSaleUtilityCls.successStatusOne,DCPNewSaleUtilityCls.detailsNotFound,DCPNewSaleUtilityCls.successStatus);
                 //Added By Pawan as part of DCP-5702 
                resp.response.maxAttorneys = string.valueOf(DCPNewSalesAddOnRequestHelper.getMaxNoOfAttorneys(requestBody.get('segment')));
            }
                return resp;
        }
        
    /********************************************************************************************************
    *  @description  getProductResponse - This is the main method to handle the final response  
    *  @return DCPProductRequestDeclaration.ProductPlanResponse  : this is a return statement and having return result as failure or success  
    *  @param productMap - expected the product parameters 
    *  @param priceMatrixMap - expected the pricing parameters 
    *********************************************************************************************************/
        
        public static DCPNewSalesAddOnRequest.ProductAddOnResponse getProductResponse(Map<String,product2> productMap,Map<String,List<Apttus_Config2__PriceMatrixEntry__c>> priceMatrixMap){
            //DCPNewSalesAddOnRequest.ProductPlanResponse res = new DCPNewSalesAddOnRequest.ProductPlanResponse();
            DCPNewSalesAddOnRequest.ProductArray prodAddON;
            DCPNewSalesAddOnRequest.ProductRule prodRule;
            //DCPNewSalesAddOnRequest.ProductAddOn finalPlan = new DCPNewSalesAddOnRequest.ProductAddOn();
            List<DCPNewSalesAddOnRequest.ProductArray> prodAddONList = new List<DCPNewSalesAddOnRequest.ProductArray>(); 
            List<DCPNewSalesYoYUtil.PriceMatrix> priceList;
            DCPNewSalesAddOnRequest.PricingDetail otherDetails;
            DCPNewSalesAddOnRequestHelper.processYOY(reqParamsMap,productMap);
            Map<String,Apttus_Config2__PriceListItem__c> pliMap = new Map<String,Apttus_Config2__PriceListItem__c>();
            pliMap = DCPNewSalesAddOnRequestHelper.getPLIInfo(productMap);
            for(Product2 prod:productMap.values()){
                prodAddON = new DCPNewSalesAddOnRequest.ProductArray();
                prodRule = new DCPNewSalesAddOnRequest.ProductRule();
                otherDetails = new DCPNewSalesAddOnRequest.PricingDetail();
                
                
                priceList = new list<DCPNewSalesYoYUtil.PriceMatrix>();
                prodAddON = DCPNewSalesAddOnRequestHelper.getProducts(prod);
                priceList = DCPNewSalesAddOnRequestHelper.getPricing(prod,priceMatrixMap);
                if(pliMap!=null && pliMap.get(prod.productCode)!=null){
                    prodAddON.priceListId = pliMap.get(prod.productCode).Apttus_Config2__PriceListId__c;
                    prodAddON.priceListItemId = pliMap.get(prod.productCode).Id;
                    otherDetails.currencyCode = pliMap.get(prod.productCode).CurrencyIsoCode;
                    otherDetails.billingFrequency = pliMap.get(prod.productCode).Apttus_Config2__BillingFrequency__c;
                    prodAddON.pricingDetails = otherDetails;
                }
                if(addonRules!=null && addonRules.size()>0){
                    prodRule = DCPNewSalesAddOnRequestUtility.createRule(prod,addonRules);
                    prodAddON.addOnRules = prodRule;
                }
             
                if(addonGroup!=null && addonGroup.size()>0){
                    
                    prodRule = DCPNewSalesAddOnRequestUtility.getRecmGroupMap(prod,addonGroup,prodRule);
                    prodAddON.addOnRules = prodRule;
                }
                prodAddON.priceMatrix = (!priceList.isEmpty())?priceList:null;
                prodAddONList.add(prodAddON);           
            }
            resp.productAddOnList = prodAddONList;
            //resp.productDetails = finalPlan;
            return resp;
        }
    }